#!/bin/bash

# Read configuration

. `dirname $0`/init.sh

# add gnukhata system user (requires adduser >= 3.34)
# don't muck around with this unless you KNOW what you're doing
if [ -f ${CONFIG} ]
then
	user=$(awk '/^user/{print $3}' "${CONFIG}")
else
	echo "${CONFIG} not found"
	exit 1
fi

function userexistmessage {

  if [ `getent passwd $user|awk -F ':' '{print $3}'` -gt 999 ] >/dev/null ; then
	echo "Non-system user $user found. I will not overwrite a non-system" >&2
	echo "user.  Remove the user and reinstall gnukhata." >&2
	exit 1
  fi
  # unknown adduser error, simply exit

}



echo "Creating/updating $user user account..."
if [ -f /etc/redhat-release ]
then 
  adduser  --system --home /var/run/$user \
         --shell /bin/false $user || {

        # adduser failed. Why?
	userexistmessage
        exit 1
   }
else
  adduser --system --home /var/run/$user \
	--gecos "$user system user" --shell /bin/false \
	--quiet --disabled-password $user || {
        # adduser failed. Why?
	userexistmessage
        exit 1
  }
fi
